#ifndef FIFOSIMULATOR_HPP
#define FIFOSIMULATOR_HPP

#include "Simulator.hpp"

class FIFOSimulator : public Simulator {
private:
	
public:
	FIFOSimulator(int numCPU, int numIO, double endTime, 
		double contextSwitchCost, double freq, double cpuBoundPct);

	void runSim();

	double findNextEvent();

};

#endif