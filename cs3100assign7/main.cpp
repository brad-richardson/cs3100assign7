#include <iostream>
#include <vector>
#include <chrono>
#include "FIFOSimulator.hpp"
#include "RRSimulator.hpp"
#include "SJFSimulator.hpp"
#include "ASJFSimulator.hpp"

int main(int argc, char* argv[])
{
	
	int cpu, ioDevices;
	double endTime, contextSwitchCost, freq, cpuBoundPct, quantumSize;
	if (argc < 7)
	{
		char choice;
		std::cout << "Use default values (y/n)? ";
		std::cin >> choice;
		if (choice == 'y')
		{
			std::cout << "Using default values to run simulations..." << std::endl;
			cpu = 8;
			ioDevices = 16;
			endTime = 10000;
			contextSwitchCost = .2;
			freq = 5;
			cpuBoundPct = 0.50;
			quantumSize = 5;
		}
		else
		{
			//allows for input from a file
			std::cout << "How many processors? ";
			std::cin >> cpu;
			std::cout << "How many IO devices? ";
			std::cin >> ioDevices;
			std::cout << "End time? ";
			std::cin >> endTime;
			std::cout << "Context switch cost? ";
			std::cin >> contextSwitchCost;
			std::cout << "Process creation frequency? ";
			std::cin >> freq;
			std::cout << "CPU bound task ratio? ";
			std::cin >> cpuBoundPct;
			std::cout << "Quantum size (Round Robin): ";
			std::cin >> quantumSize;
		}
	}
	else if (argc == 7)
	{
		cpu = atoi(argv[1]);
		ioDevices = atoi(argv[2]);
		endTime = atoi(argv[3]);
		contextSwitchCost = atof(argv[4]);
		freq = atof(argv[5]);
		cpuBoundPct = atof(argv[6]);
		quantumSize = atof(argv[7]);
	}
	//auto start = std::chrono::system_clock::now();
	
	FIFOSimulator fifosim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct);
	std::cout << fifosim.toString();

	RRSimulator rrsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct, quantumSize);
	std::cout << rrsim.toString();

	SJFSimulator sjfsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct);
	std::cout << sjfsim.toString();

	ASJFSimulator asjfsim(cpu, ioDevices, endTime, contextSwitchCost, freq, cpuBoundPct);
	std::cout << asjfsim.toString();

	return 0;
}