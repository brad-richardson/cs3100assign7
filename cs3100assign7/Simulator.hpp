#ifndef SIMULATOR_HPP
#define SIMULATOR_HPP

#include <iostream>
#include <deque>
#include <vector>
#include <string>
#include <sstream>
#include <random>
#include <algorithm>
#include "process.hpp"
#include "tools.hpp"

//Base class
class Simulator {
protected:
	std::string algorithm;
	int numCPU;
	int numIO;
	int processesSpawned = 0;
	//determines how many tasks(or bursts) per process
	int minTasks = 2;
	int maxTasks = 10;

	double currTime=0;
	double endTime;

	double contextSwitchCost;
	double freq;
	double timeToNextSpawn;
	double cpuBoundPct;
	std::vector<double> utilization;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> taskDist;
	std::bernoulli_distribution cpuBoundDist;

	std::deque<std::shared_ptr<Process>> readySet;
	std::vector<std::shared_ptr<Process>> executeSet;
	std::vector<std::deque<std::shared_ptr<Process>>> waitSet;
	std::vector<std::shared_ptr<Process>> doneSet;

public:
	Simulator(int numCPU, int numIO, double endTime,
		double contextSwitchCost, double freq, double cpuBoundPct, std::string algorithm);

	virtual void runSim()=0;

	virtual double findNextEvent()=0;

	std::string printStats();

	std::string toString();

};

#endif