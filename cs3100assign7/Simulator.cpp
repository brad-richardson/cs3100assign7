#include "Simulator.hpp"

Simulator::Simulator(int numCPU, int numIO, double endTime,
	double contextSwitchCost, double freq, double cpuBoundPct, std::string algorithm)
	: numCPU(numCPU), numIO(numIO), endTime(endTime),
	contextSwitchCost(contextSwitchCost), freq(freq), 
	cpuBoundPct(cpuBoundPct), algorithm(algorithm)
{
	timeToNextSpawn = freq;
	taskDist = std::uniform_int_distribution<int>(minTasks, maxTasks);
	if (cpuBoundPct > 1 || cpuBoundPct < 0)
	{
		std::cout << "Invalid CPU bound pct, using 0.5..." << std::endl;
		cpuBoundPct = 0.5;
	}
	cpuBoundDist = std::bernoulli_distribution(cpuBoundPct);
	waitSet.resize(numIO);
	//?? here or in child?
	//runSim();
	//std::cout << printStats();
}

std::string Simulator::printStats()
{
	std::stringstream ss;
	ss << std::endl << "Stats: " << std::endl;
	ss << "Processes finished/created: " << doneSet.size() << "/" << processesSpawned << std::endl;
	std::vector<double> responseVals;
	for (std::shared_ptr<Process> p : readySet)
	{
		responseVals.push_back(p.get()->getResponse());
	}
	for (std::shared_ptr<Process> p : executeSet)
	{
		responseVals.push_back(p.get()->getResponse());
	}
	for (std::deque<std::shared_ptr<Process>> &set : waitSet)
	{
		for (std::shared_ptr<Process> p : set)
		{
			responseVals.push_back(p.get()->getResponse());
		}
	}
	for (std::shared_ptr<Process> p : doneSet)
	{
		responseVals.push_back(p.get()->getResponse());
	}
	if ((int) responseVals.size() > 0 && mean(responseVals)> 0)
	{
		ss << "Response - avg: " << mean(responseVals)
			<< " min: " << findMin(responseVals) << " max: " << findMax(responseVals)
			<< " sd: " << stdDev(responseVals) << std::endl;
	}
	else
	{
		ss << "Response - no processes completed any cycles" << std::endl;
	}
	std::vector<double> latencyVals;
	for (std::shared_ptr<Process> p : doneSet)
	{
		latencyVals.push_back(p.get()->getLatency());
	}
	if (latencyVals.size() > 0)
	{
		ss << "Latency - avg: " << mean(latencyVals)
			<< " min: " << findMin(latencyVals) << " max: " << findMax(latencyVals)
			<< " sd: " << stdDev(latencyVals) << std::endl;
	}
	else
	{
		ss << "Latency - no processes finished" << std::endl;
	}

	ss << "CPU utilization: " << mean(utilization) * 100 << "%" << std::endl;
	ss << "Throughput: " << doneSet.size() * 100 / endTime << " processes/100 time" << std::endl;
	ss << "Processes still in ready set: " << readySet.size() << std::endl;
	ss << "Processes still in execute set: " << executeSet.size() << std::endl;
	int processesInWaitSet = 0;
	for (std::deque<std::shared_ptr<Process>> &set : waitSet)
	{
		for (std::shared_ptr<Process> p : set)
		{
			processesInWaitSet++;
		}
	}
	ss << "Processes still in wait set: " << processesInWaitSet << std::endl << std::endl;

	return ss.str();
}

std::string Simulator::toString()
{
	std::stringstream ss;
	ss << "Simulator settings: " << std::endl;
	ss << "Algorithm: " << algorithm << std::endl;
	ss << "CPUs: " << numCPU << std::endl;
	ss << "IO devices: " << numIO << std::endl;
	ss << "End time: " << endTime << std::endl;
	ss << "Context switch cost: " << contextSwitchCost << std::endl;
	ss << "Process generation frequency: " << freq << std::endl;
	ss << "CPU bound task ratio: " << cpuBoundPct << std::endl;
	return ss.str();
}