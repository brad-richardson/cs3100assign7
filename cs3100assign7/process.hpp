#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <queue>
#include <string>
#include <sstream>
#include <memory>
#include <random>
#include "tools.hpp"

typedef std::pair<double, double> Task;

class Process
{
private:
	std::deque<Task> tasks;
	std::deque<int> ioDeviceToUse;
	std::vector<double> prevCPUTimes;
	double prevCPUTimeAvg;

	bool cpuBound;
	int ioDevices;
	double initTime = -1;
	double response = -1; //time between init and first cycle completion
	double latency = -1; //time between creation and completion	

public:
	Process(int numOfTasks=1, bool cpuBound=false, int ioDevices=1, double currTime=0);
	
	Task getTask();
	//for ASJF, keep track of average cpu time
	void storeCPUTime();
	double avgCPUTime();
	void subtractCPUTime(double time);
	void addCPUTime(double time);
	void subtractIOTime(double time);
	double getCurrCPU();
	double getCurrIO();
	int getCurrIODevice();
	double getResponse();
	double getLatency();
	bool isCPUBound();
	bool isDone();

	void doCycle(double currTime);

	std::string toString();

};

#endif